# Fortran Project no. 2

Project no. 2 at *Programming in Fortran Language* Course.
Detail information about the task can be found [here](http://home.agh.edu.pl/~macwozni/fort/projekt2.pdf).

## Compilator

Compiled with **gfortran v. 5.5.0**

Unit tests run with **pFUnit 3.2.9**

Make sure to export flags: *PFUNIT*, *F90_VENDOR*, *F90*, *COMPILER* to make pFUnit working.

## Running tests

To run tests, type:
```
make taskN_Test
```
where **N** is number of task:

* `task1_Test` - version without optimisation
* `task2a_Test` - version with *dot_product* optimisation
* `task2b_Test` - version with *ichunk* optimisation
* `task2c_Test` - version with *dot_product* and *ichunk* optimisation

It will produce `./build/test` and run it; tests for chosen version of module will be performed.

## Task 3

To run task 3, type:

```
make task3
```

It will create and run programs measuring time of multipling for each version.
Results will be written into files:

* `Task3/result/ver1` - version without optimisation
* `Task3/result/ver2a` - version with *dot_product* optimisation
* `Task3/result/ver2b` - version with *ichunk* optimisation
* `Task3/result/ver2c` - version with *dot_product* and *ichunk* optimisation
* `Task3/result/ver3` - version using build-in function *matmul*


### Graph
Graphs illustrating program results can be found in `Task3/result/` directory.