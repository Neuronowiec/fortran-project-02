task1_Test:
	make -C test test MODPATH='../Task1'
	./bin/test

task2a_Test:
	make -C test test MODPATH='../Task2a'
	./bin/test

task2b_Test:
	make -C test test MODPATH='../Task2b'
	./bin/test

task2c_Test:
	make -C test test MODPATH='../Task2c'
	./bin/test

task3:
	mkdir -p ./bin
	mkdir -p ./Task3/result
	make -C Task3 performanceTest MODPATH='../Task1'
	./bin/performanceTest > ./Task3/result/ver1
	make -C Task3 performanceTest MODPATH='../Task2a'
	./bin/performanceTest > ./Task3/result/ver2a
	make -C Task3 performanceTest MODPATH='../Task2b'
	./bin/performanceTest > ./Task3/result/ver2b
	make -C Task3 performanceTest MODPATH='../Task2c'
	./bin/performanceTest > ./Task3/result/ver2c
	make -C Task3 performanceTest MODPATH='../Task3'
	./bin/performanceTest > ./Task3/result/ver3
	
clean:
	make -C test clean
	make -C Task3 clean